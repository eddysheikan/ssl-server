
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var fs = require('fs');
var https = require('https');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

var opts = {
   
  // Key file para servidor.
  key: fs.readFileSync('server.key'),
   
  // Certificado.
  cert: fs.readFileSync('server.cer'),
   
  // Certificado de autoridad certificadora (self-signed).
  ca: fs.readFileSync('ca.cer'),
   
  // Pedir al cliente que proporcione un certificado.
  requestCert: true,
   
  // No permitir tráfico no autorizado.
  rejectUnauthorized: true
};

https.createServer(opts, app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
